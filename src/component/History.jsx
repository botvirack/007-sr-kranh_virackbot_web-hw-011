import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Table, Button } from "react-bootstrap";

export default function History(props) {
  return (
    <div>
      <h2>Result History <Button onClick={props.clear} variant="warning"><b>Clear</b></Button>{' '}</h2>
      <Table striped bordered hover>
        <tbody>{props.getValue}</tbody>
      </Table>
    </div>
  );
}
