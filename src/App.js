import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Calculator from "./component/Calculator";
function App() {
  return (
    <div className="App" >
      <Calculator />
    </div>
  );
}

export default App;
