import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Card, Button, Form } from "react-bootstrap";
import History from "./History";
import "../App.css";

export default class Calculator extends Component {
  constructor() {
    super();
    this.state = {
      val1: "",
      val2: "",
      total: 0,
      operator: "+",
      resultList: [],
      item: null,
      msg: "",
      EmptyRequired: "Please input value to field!",
      msgNumber: "Can only input number!",
      valTerm1: null,
      valTerm2: null,
    };
  }

  clearTable = () => {
    this.setState({
      resultList: [],
      item: null,
    })
  }

  handleChange(event) {
    this.setState(
      {
        val1: event.target.value,
      },
      () => {
        const reg = /[^0-9]/gi;
        this.setState({
          valTerm1: reg.test(this.state.val1),
        });
      }
    );
  }

  handleChange2(event) {
    this.setState(
      {
        //[event.target.name]:event.target.value,
        val2: event.target.value,
      },
      () => {
        const reg = /[^0-9]/gi;
        this.setState({
         valTerm2: reg.test(this.state.val2),
        });
      }
    );
  }

  calculate = () => {
    console.log("VALUE1:", this.state.valTerm1);
    console.log("VALUE2:", this.state.valTerm2);
    this.setState(
      () => {
        if (
          this.state.val1 !== "" &&
          this.state.val2 !== "" &&
          this.state.valTerm1 === false &&
          this.state.valTerm2 === false
        ) {
          if (this.state.operator === "+") {
            return {
              total: parseInt(this.state.val1) + parseInt(this.state.val2),
            };
          } else if (this.state.operator === "-") {
            return {
              total: parseInt(this.state.val1) - parseInt(this.state.val2),
            };
          } else if (this.state.operator === "*") {
            return {
              total: parseInt(this.state.val1) * parseInt(this.state.val2),
            };
          } else if (this.state.operator === "/") {
            return {
              total: parseInt(this.state.val1) / parseInt(this.state.val2),
            };
          } else if (this.state.operator === "%") {
            return {
              total: parseInt(this.state.val1) % parseInt(this.state.val2),
            };
          }
        }
      },
      () => {
        if (
          this.state.val1 !== "" &&
          this.state.val2 !== "" &&
          this.state.valTerm1 === false &&
          this.state.valTerm2 === false
        ) {
          this.state.resultList.push(this.state.total);
          this.setState({
            msg: "",
            val1: "",
            val2: "",
            item: this.state.resultList.map((e, index) => (
              <tr key={index}>
                <td>{String(e)}</td>
              </tr>
            )),
          });
        } else {
          this.setState({
            msg: this.state.EmptyRequired,
          });
        }
      }
    );
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <Card style={{ width: "22rem", margin: "10px" }}>
              <Card.Img
                variant="top"
                src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/GNOME_Calculator_icon_2018.svg/1000px-GNOME_Calculator_icon_2018.svg.png"
              />
              <Card.Body >
                <Card.Title>Calculator</Card.Title>
                <Form>
                  <Form.Group controlId="formBasicEmail"> 
                    <Form.Control
                      type="text"
                      placeholder="Enter value 1"
                      name="val1"
                      value={this.state.val1}
                      onChange={this.handleChange.bind(this)}
                    />
                    <Form.Text className="alert-sms text-danger">
                      &nbsp;
                      {this.state.val1 === ""
                        ? this.state.msg
                        : this.state.valTerm1 === true
                        ? this.state.msgNumber
                        : ""}
                    </Form.Text>
                  </Form.Group>
                  <Form.Group controlId="formBasicEmail">
                    <Form.Control
                      type="text"
                      placeholder="Enter value 2"
                      name="val2"
                      value={this.state.val2}
                      onChange={this.handleChange2.bind(this)}
                    />
                    <Form.Text className="alert-sms text-danger">
                      &nbsp;
                      {this.state.val2 === ""
                        ? this.state.msg
                        : this.state.valTerm2 === true
                        ? this.state.msgNumber
                        : ""}
                    </Form.Text>
                  </Form.Group>
                  <Form.Group controlId="exampleForm.ControlSelect1">
                    <Form.Control as="select" onChange={(e) => this.setState({ operator: e.target.value })} >
                      <option value="+">+ Plus</option>
                      <option value="-">- Subtract</option>
                      <option value="*">* Multiply</option>
                      <option value="/">/ Divide</option>
                      <option value="%">% Module</option>
                    </Form.Control>
                  </Form.Group>
                  <Button id="btn-cal" variant="primary" type="button" onClick={this.calculate}>
                    Calculate
                  </Button>
                </Form>
              </Card.Body>
            </Card>
          </div>
          <div className="col-md-6">
            <History getValue={this.state.item} clear={this.clearTable} />
          </div>
        </div>
      </div>
    );
  }
}
